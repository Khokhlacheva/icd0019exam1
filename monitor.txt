Failis monitor.jar on kood, mis iga 5 sekundi järel teie tehtud töö ära saadab.
Seega peaksite enne eksamiga alustamist käivitama käsu.

   java -jar monitor.jar <teie Bitbucket-i kasutajatunnus>

Käsu andmiseks peate olema selles kaustas, kuhu te eksami projekti kloonisite.
Käsk avab akna, kus on "Start" nupp. Kui sellele vajutate peaks failide jälgija
tööle hakkama ja peaks teatama "Last upload at ...". Kui kõik on hästi, peaks teates
olev kellaaeg iga 5 sekundi järel uuenema.

Kui seda "Last upload at ..." kirja mingil põhjusel ei tule või alguses tuleb ja
hiljem avastate, et kellaaeg pole uuenenud, siis peaksite tehtud töö ise üles laadima.

Töö ise üles laadimseks peaksite kopeerima kataloogid ex1 ja ex2
ning faili info.txt oma projekti (icd0019) reposse kataloogi nimega exam.
